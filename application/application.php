<?php

/**
 * Main application functions
 * Author: Jesse Malotaux <jemx@mboutrecht>
 */

function start_application()
{
    $urlArr = url_parse();
    less_compile();

    if ($urlArr['pagename'] == 'function') {
        include BASEDIR . 'application/function/' . $urlArr['pagevars'][0] . '-functions.php';
        if (count($urlArr['pagevars']) < 3) {
            $urlArr['pagevars'][1]();
        } else {
            $urlArr['pagevars'][1](array_slice($urlArr['pagevars'], 2));
        }
    } else {
        get_pagecontent($urlArr);
    }
}

/**
 *
 */
function url_parse()
{
    $baseDir = str_replace('application/index.php', '', $_SERVER['SCRIPT_FILENAME']);
    $baseUrl = 'computershop/application/';

    define('BASEDIR', $baseDir);

    $varStr = str_replace($baseUrl, '', (isset($_SERVER['REDIRECT_URL']) ? $_SERVER['REDIRECT_URL'] : $_SERVER['REQUEST_URI']));
    $urlVars = array_values(array_filter((strpos($varStr, '/') !== false ? explode('/', $varStr) : array($varStr))));
    $count = count($urlVars);

    $pageVars = array();

    if (strpos(end($urlVars), '.') !== false) {
        return null;
    }

    if ($count > 0) {
        for ($i = 0; $i < $count; $i++) {
            if (!empty($urlVars[$i])) {
                if ($i == 0) {
                    $pageName = $urlVars[$i];
                } else {
                    $pageVars[$i] = $urlVars[$i];
                }

            }
        }
    }

    if (!empty($pageName)) {
        $pageVars = array_values($pageVars);
        $urlArr = array(
            'pagename' => $pageName,
            'pagevars' => $pageVars
        );
    } else {
        $urlArr = array(
            'pagename' => 'home',
            'pagevars' => array()
        );
    }

    return $urlArr;

}

function less_compile()
{
    include BASEDIR . 'application/lib/lessc.inc.php';

    $lessc = new lessc;

    $lessc->setFormatter( "compressed" );

    $lessDir = BASEDIR . 'lib/less/';
    $cssDir = BASEDIR . 'lib/css/';

    $scanLess = scandir($lessDir);

    foreach ( $scanLess as $filename ) {
        if ($filename !== '.' && $filename !== '..') {
            $lessFile = $lessDir . $filename;
            $cssFile = $cssDir . str_replace( '.less', '.css', $filename );

            if ( file_exists( $lessFile ) ) {
                try {
                    $lessc->checkedCompile( $lessFile, $cssFile );
                } catch ( Exception $e ) {
                    echo 'lessphp fatal error: ' . $e->getMessage();
                }

            } else {
                echo 'less file doesn\'t exist: ' . $lessFile . '<br>';
            }
        }
    }
}

function get_pagecontent($urlArr)
{
    include BASEDIR . 'application/inc/db.php';
    $db = new db();

    $pageQuery = $db->query('SELECT id, title FROM pages WHERE page_url = :pagename;');
    $db->bind(':pagename', $urlArr['pagename']);
    $db->execute();
    $pageResult = $db->single();

    if (empty($pageResult)) {
        echo json_encode('400');
    } else {
        $contentQuery = $db->query('SELECT content_title, content FROM page_content WHERE page_id = :pageid ORDER BY page_content_order;');
        $db->bind(':pageid', $pageResult['id']);
        $db->execute();
        $contentResult = $db->resultset();


        echo json_encode(format_content($contentResult));
    }

}

function format_content($contentArr)
{
    $formatArr = array();
    foreach ($contentArr as $key => $content) {
        $formatArr[$key]['content-title'] = $content['content_title'];
        $formatArr[$key]['content'] = $content['content'];
    }
    return $formatArr;
}