<?php

/*
 * Menu functions
 * Author: Jesse Malotaux <jemx@mboutrecht.nl>
 */

function get_structure()
{
    include BASEDIR . 'application/inc/db.php';
    $db = new db();

    $pageQuery = $db->query('SELECT * FROM pages ORDER BY page_order');
    $db->execute();
    $pageResult = $db->resultset();

    $pageArr = array();
    $orderArr = array();

    foreach ($pageResult as $page) {
        //print_r($page);
        if (empty($page['parent_id'])) {
            $orderArr[$page['page_order']] = $page['id'];
            $pageArr[$page['id']]['title'] = $page['title'];
            $pageArr[$page['id']]['url'] = $page['page_url'];
        } else {
            $pageArr[$page['parent_id']]['subpages'][$page['id']]['title'] = $page['title'];
            $pageArr[$page['parent_id']]['subpages'][$page['id']]['url'] = $page['page_url'];
        }
    }

    $pageOrderArr = array();

    foreach ($orderArr as $order) {
        $pageOrderArr[] = $pageArr[$order];
    }

    echo json_encode($pageOrderArr);
}