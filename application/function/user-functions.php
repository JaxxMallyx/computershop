<?php
error_reporting(E_ALL | E_STRICT);
ini_set("display_errors",1);
/*
 * User functions
 * Author: Jesse Malotaux <jemx@mboutrecht.nl>
 */

function check_username() {

    require_once BASEDIR . 'application/inc/db.php';
    $db = new db();

    $db->query('SELECT user_id FROM users WHERE username = :username');
    $db->bind(':username', $_POST['username']);
    $db->execute();
    $checkResult = $db->single();

    if (isset($checkResult['user_id'])) {
        echo 1;
    } else {
        echo 0;
    }

}

function check_email() {

    require_once BASEDIR . 'application/inc/db.php';
    $db = new db();

    $db->query('SELECT user_id FROM users WHERE email = :email');
    $db->bind(':email', $_POST['email']);
    $db->execute();
    $checkResult = $db->single();

    if (isset($checkResult['user_id'])) {
        echo 1;
    } else {
        echo 0;
    }
}

function register() {

    require_once BASEDIR . 'application/inc/db.php';
    $db = new db();

    $db->query("INSERT INTO users(username, firstname, lastname, email, street, housenumber, city, postal, role) VALUES (:username,:firstname,:lastname,:email,:street,:housenumber,:city,:postal,1)");

    $db->bind(':username', strtolower($_POST['username']));
    $db->bind(':firstname', ucfirst(strtolower($_POST['firstname'])));
    $db->bind(':lastname', $_POST['lastname']);
    $db->bind(':email', strtolower($_POST['email']));
    $db->bind(':street', $_POST['street']);
    $db->bind(':housenumber', $_POST['housenumber']);
    $db->bind(':city', ucfirst(strtolower($_POST['city'])));
    $db->bind(':postal', strtoupper($_POST['postal']));

    try {
        $db->execute();
        $newUserId = $db->lastInsertId();
    } catch (Exception $e) {
        $newUserId = false;
    }

    if ($newUserId) {
        $db->query("INSERT INTO user_pass(user_id, password) VALUES (:userId,:password)");
        $db->bind(':userId', $newUserId);
        $db->bind(':password', hash('sha512', $_POST['password']));
        $db->execute();
        return $newUserId;
    } else {
        echo 'false';
    }

}

function login() {

    require_once BASEDIR . 'application/inc/db.php';
    $db = new db();

    $db->query('SELECT * FROM users WHERE username = :username');
    $db->bind(':username', strtolower($_POST['username']));
    $db->execute();
    $userResult = $db->resultset();

    if (empty($userResult)) {
        echo 0;
    } else {
        $userArr = $userResult[0];

        $db->query('SELECT password FROM user_pass WHERE user_id = :id');
        $db->bind(':id', $userArr['user_id']);
        $db->execute();
        $passResult = $db->single();
        $hashPass = hash('sha512', $_POST['password']);

        if (empty($passResult)) {
            echo 0;
        } else if ($passResult['password'] == $hashPass) {
            session_start();
            $_SESSION['user'] = $userArr;
            echo 1;
        } else {
            echo 0;
        }
    }

}

function logout() {
    session_start();
    session_destroy();
    if (!isset($_SESSION['user'])) {
        echo '1';
    } else {
        echo '0';
    }
}

function check_user() {
    session_start();
    if (isset($_SESSION['user'])) {
        echo json_encode($_SESSION['user']);
    } else {
        echo 0;
    }
}