$(document).ready(function () {
    user = {}
    // pageActive Helper: returns active if page-url is the same as the current pagename.
    Handlebars.registerHelper('pageActive', function (url) {
        url = Handlebars.Utils.escapeExpression(url);
        if (url == pageName) {
            return 'active';
        }
    });

// menuLink Helper: returns a menulink with corresponding url, title and Bootstrap classes.
    Handlebars.registerHelper('menulink', function (sub = false)
    {
        text = Handlebars.Utils.escapeExpression(this.title);
        url = Handlebars.Utils.escapeExpression(baseUrl + this.url);

        if (typeof this.subpages == 'undefined') {
            var linkclass = (typeof sub == "boolean" && sub == true ? 'dropdown-item' : 'nav-link'), // Check if menu item is a subpage
                result = '<a class="' + linkclass + '" href="' + url + '">' + text + '</a>'; // If the item is a subpage it will get the class "dropdown-item" else "nav-link".
        } else {
            var result = '<a class="dropdown-toggle nav-link" id="dropdown-' + url + '" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' + text + '</a>'; //dropdown-toggle link
        }
        return new Handlebars.SafeString(result);
    });

    Handlebars.registerHelper('formatPrice', function (price) {
        var splitPrice = price.split('.'),
            strPrice = '<h2>'+splitPrice[0]+'</h2>';
            decimal = parseFloat('0.'+splitPrice[1]).toFixed(2);
            strPrice += '<sup>'+decimal.toString().replace('0.', ',')+'</sup>';
        return strPrice;
    });

    Handlebars.registerHelper('shortDescription', function (description) {
        var maxLength = 50,
            trimmedString = description.substr(0, maxLength);

        trimmedString = trimmedString.substr(0, Math.min(trimmedString.length, trimmedString.lastIndexOf(" ")))+"... ";
        return trimmedString;
    });

    Handlebars.registerHelper('userLinks', function () {
        var html = '';
        check_user((function (data) { html = data; }));
        return html;
    });
});

function check_user(callback) {
    var html = '';
    $.ajax({
        url: baseUrl + 'application/function/user/check_user/',
        method: 'post',
        async: false
    }).done(function(data) {
        var check = JSON.parse(data);
        if (typeof check == 'object') {
            user = check;
            html += '<p>Welkom '+check.firstname+' '+check.lastname+'</p>';
            html += '<li class="nav-item"><a class="nav-link" href="'+baseUrl+'profile"><i class="fa fa-user" aria-hidden="true"></i>Profiel</a></li>';
            html += '<li class="nav-item"><a class="nav-link" href="'+baseUrl+'shoppingcart"><div id="cartAmount"></div><i class="fa fa-shopping-cart" aria-hidden="true"></i>Winkelwagen</a></li>';
            html += '<li class="nav-item"><a class="nav-link" href="'+baseUrl+'logout"><i class="fa fa-power-off" aria-hidden="true"></i>Uitloggen</a></li>';
        } else if (check == 0) {
            html += '<li class="nav-item"><a class="nav-link" href="'+baseUrl+'login"><i class="fa fa-sign-in" aria-hidden="true"></i>Login</a></li>';
            html += '<li class="nav-item"><a class="nav-link" href="'+baseUrl+'register"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Register</a></li>';
        }
        callback(html);
    });
}