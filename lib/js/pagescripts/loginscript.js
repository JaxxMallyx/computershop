$(document).ready(function() {

    $('body').delegate('form#login-form', 'submit', function (e) {
        e.preventDefault();
        var formVals = {};

        $.each($(this).find('input'), function() {
            formVals[$(this).attr('id')] = $(this).val();
        });

        login_user(formVals);
    });

});

function login_user(formVals) {
    $.ajax({
        url: baseUrl + 'application/function/user/login/',
        method: 'post',
        data: formVals,
    }).done(function (data) {
        if (data == '0') {
            $('form#login-form .alert').removeClass('hidden');
        } else if (data == '1') {
            window.location.href = baseUrl;
        }
    });
}