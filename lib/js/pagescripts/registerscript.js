$(document).ready(function() {
    $('body').delegate('input#username', 'change', function() {
        check_username($(this));
    });
    $('body').delegate('input#email', 'change', function() {
        check_email($(this));
    });
    $('body').delegate('form#register-form', 'submit', function (e){
        e.preventDefault();
        var formVals = {};

        $.each($(this).find('input'), function() {
            formVals[$(this).attr('id')] = $(this).val();
        });

        register_user(formVals);
    });
});

function check_username(element) {
    $.ajax({
        url: baseUrl + 'application/function/user/check_username/',
        method: 'post',
        data: {username: element.val()},
    }).done(function (data) {
        if (data == 1) {
            element.addClass('is-invalid');
        } else {
            element.removeClass('is-invalid');
        }
    });
}

function check_email(element) {
    $.ajax({
        url: baseUrl + 'application/function/user/check_email/',
        method: 'post',
        data: {email: element.val()},
    }).done(function (data) {
        if (data == 1) {
            element.addClass('is-invalid');
        } else {
            element.removeClass('is-invalid');
        }
    });
}

function register_user(formVals) {
    $.ajax({
        url: baseUrl + 'application/function/user/register/',
        method: 'post',
        data: formVals,
    }).done(function (data) {
        //console.log(formVals);
        console.log(data);
    });
}