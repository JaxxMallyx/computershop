$(document).ready(function() {
    if (typeof Cookies.get('cart') === 'undefined') {
        $('#shoppingcart').html('<h1>Winkelwagen is leeg</h1><p>Klik <a href="'+baseUrl+'shop">hier</a> om te gaan winkelen!</p>').addClass('empty-cart');
    } else {
        initiate_cart();
    }
});

function initiate_cart() {
    // get_menu_structure(function (menuStructure) {
    //     menuContent = {baseurl: baseUrl, menustructure: menuStructure};
    // });
    //
    // get_parts('main_menu', (function(parts){ mainmenu = parts; }), 'part');
    // get_parts(pageName, (function(parts){ pageContent = parts; }), 'page');
    //
    // html_compile(mainmenu, menuContent, false, (function (html) {
    //     contentObj.main_menu = html;
    // }));
    var cartContent = $('table#shoppingcart'),
        cart = JSON.parse(Cookies.get('cart')),
        cartObj = [],
        idArr = [],
        cartHtml = '';

    for (var id in cart.products) {
        if (typeof cart.products[id] === 'number') {
            idArr.push(id);
        }
    }

    get_parts('shoppingcart-row', (function(parts){ cartHtml = parts; }), 'part');

    get_products(idArr, (function(data) {
        cartObj = {products: data};
    }));

    html_compile(cartHtml, cartObj, false, (function (data) { console.log(data); }));
    //
    // get_parts('main_menu', (function(parts){ mainmenu = parts; }), 'part');
    // get_parts(pageName, (function(parts){ pageContent = parts; }), 'page');
    //
    // html_compile(mainmenu, menuContent, false, (function (html) {
    //     contentObj.main_menu = html;
    // }));
}

function get_products(idArr, callback) {
    $.ajax({
        url: baseUrl + 'application/function/product/get_products_by_id/',
        type: 'post',
        data: {productIds: idArr},
        async: true
    }).done(function(data) {
        callback(JSON.parse(data));
    });
}