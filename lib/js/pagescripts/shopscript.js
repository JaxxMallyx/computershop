$(document).ready(function() {

    if ($('#products-maincontent').length > 0) {
        var mainContent = $('#products-maincontent'),
            htmlStr = '',
            contentObj = {};
            compileHtml = '';

        if (pageVars.length > 1) {
            switch (pageVars[0]) {
                case 'product':
                    var products = {};

                    pageVars.shift();

                    get_parts('product-detail', ( function(data) { htmlStr = data; }));
                    get_products((function(data){ products = data; }), 'product', pageVars);

                    contentObj = {products: products};
                    break;
                case 'category':
                    var categories = {},
                        products = {};

                    pageVars.shift();

                    get_parts('products-overview', ( function(data) { htmlStr = data; }));
                    get_categories((function(data){ categories = data; }));
                    get_products((function(data){ products = data; }), 'category', pageVars);

                    contentObj = {categories: categories, products: products};
                    break;
            }
        } else {
            var categories = {},
                products = {};

            get_parts('products-overview', ( function(data) { htmlStr = data; }));
            get_categories((function(data){ categories = data; }));
            get_products((function(data){ products = data; }));

            contentObj = {categories: categories, products: products};
        }

        html_compile(htmlStr, contentObj, false, (function (data) { compileHtml = data; }));
        mainContent.html(compileHtml);
    }


});

function get_products(callback, by = null, identifier = []) {

    identifier = identifier.join('/');

    switch (by) {
        case 'category':
            var productUrl = baseUrl + 'application/function/product/get_products_by_category/'+identifier;
            break;
        case 'product':
            var productUrl = baseUrl + 'application/function/product/get_products_by_id/'+identifier;
            break;
        default:
            var productUrl = baseUrl + 'application/function/product/get_products/';
            break;
    }

    $.ajax({
        url: productUrl,
        type: 'post',
        //data: {baseurl: baseUrl, pagename: pageName, pagevars: pageVars},
        async: false
    }).done(function (data) {
        callback(JSON.parse(data));
    });
}

function get_categories(callback) {
    var categoriesContainer = $('#categories-container');
    categoriesContainer.addClass('loading-content');
    $.ajax({
        url: baseUrl + 'application/function/product/get_categories/',
        type: 'post',
        //data: {baseurl: baseUrl, pagename: pageName, pagevars: pageVars},
        async: false
    }).done(function (data) {
        callback(JSON.parse(data));
    });
}