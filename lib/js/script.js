$(document).ready(function () {
	url_parse();

    page_script();

    element_compile($('#tophead'), {'pageName': pageName});

    main_content();

    setTimeout(function () {
        $('body.loading-content').removeClass('loading-content');
    }, 500);

    $('body').delegate('.collapse-container .collapse-trigger', 'click', function() {
        if ($(window).width() < 768) {
            var trigger = $(this),
                container = trigger.parents('.collapse-container'),
                content = container.find('.collapse-content'),
                height = content.children().outerHeight();

            if (content.hasClass('open') === false) {
                content.animate({height: height}, 250, 'swing').addClass('open');
            } else {
                content.animate({height: 0}).removeClass('open');
            }
        }
    });

    $('body').delegate('.add-to-cart', 'click', function() {
        if ($.isEmptyObject(user)) {
            window.location.href = baseUrl + 'login';
        } else {
            add_to_cart($(this).data('productid'), 1, $(this).data('price'));
        }
    });

    $('html').delegate('body', 'keydown', function (e) {
        var evtobj = window.event ? event : e;
        if (evtobj.keyCode == 81 && evtobj.ctrlKey) {
            Cookies.remove('cart');
            console.log('cookie removed');
        }
    });

    if (typeof Cookies.get('cart') === 'string') {
        var cart = JSON.parse(Cookies.get('cart'));
        update_cart_amount(cart.amount);
    }
});

function url_parse()
{
	var websiteName = 'computershop',
		location = document.location.href
		nameIndex = location.indexOf(websiteName) + websiteName.length + 1;

		baseUrl = location.substr(0, nameIndex);

	var varStr = location.replace(baseUrl, ''),
		urlVars = varStr.split('/'),
		count = urlVars.length;

		pageVars = [];

	if (count > 0) {
		for (var i = 0; i < count; i++) {
			if (urlVars[i].length > 0) {
				if (i == 0) {
					pageName = urlVars[i].replace(/[|&;$%@#?"<>()+,]/g, "");
				} else {
					pageVars[i] = urlVars[i].replace(/[|&;$%@#?"<>()+,]/g, "");
				}
			} else if (i == count-1 && count == 1) {
                pageName = 'home';
            }
		}
	} else {
        pageName = 'home';
	}
    if (pageName == 'logout') {
	    logout();
    }
	pageVars = pageVars.filter(function(){ return true; });

}

function logout() {
    $.ajax({
        url: baseUrl + 'application/function/user/logout/',
        method: 'post'
    }).done(function (data) {
        console.log(data);
        if (data == '0') {
            $('div#logout-message').removeClass('alert-info').addClass('alert-danger').text('Er is iets fout gegaan, probeer het later opnieuw.');
        } else if (data == '1') {
            window.location.href = baseUrl;
        }
    });
}

function page_script() {
    $.ajax({
        url: baseUrl + 'lib/js/pagescripts/' + pageName + 'script.js',
        type: 'HEAD',
        async: false,
        success: function () {
            $('#mainscript').after('<script src="/computershop/lib/js/pagescripts/' + pageName + 'script.js"></script>');
        }
    });
}

function element_compile(element, content, formatted = false) {

	var template = element.html(),
		templateScript = Handlebars.compile(template);
    console.log(template);
	if (!formatted) {
        var html = templateScript(content, {noEscape: true});
    } else {
        var html = $('<div/>').html(templateScript(content)).text();
	}

	element.html(html);

    if (element.hasClass('loading-content') === true) {
        element.removeClass('loading-content');
    }
}

function html_compile(htmlStr, content, formatted = false, callback = null) {
    var templateScript = Handlebars.compile(htmlStr);

    if (!formatted) {
        var html = templateScript(content);
    } else {
        var html = templateScript(content, {noEscape: true});
    }

    if (typeof callback !== 'function') {
        callback.html(html);
    } else {
        callback(html);
    }
}

function main_content() {
	var mainContent = $('div#mainContent'),
        contentObj = {},
        menuContent = {},
        mainmenu = '',
        pageContent = '';

    get_menu_structure(function (menuStructure) {
        menuContent = {baseurl: baseUrl, menustructure: menuStructure};
    });

    get_parts('main_menu', (function(parts){ mainmenu = parts; }), 'part');
    get_parts(pageName, (function(parts){ pageContent = parts; }), 'page');

    html_compile(mainmenu, menuContent, false, (function (html) {
        contentObj.main_menu = html;
    }));

    contentObj.pageContent = pageContent;
    element_compile(mainContent, contentObj, true);

    if ($('#main-container').length) {
        load_maincontent($('#main-container'));
    }
}

function get_menu_structure(callback) {
    $.ajax({
        url: baseUrl + 'application/function/menu/get_structure/',
        type: 'get',
        data: {baseurl: baseUrl, pagename: pageName, pagevars: pageVars},
        async: false
    }).done(function (data) {
        callback(JSON.parse(data));
    });
}

function get_parts(name, callback, type = 'part') {
    var partUrl = baseUrl + (type == 'part' ? 'parts/' : 'pages/') + name.toLowerCase() + '.html';

    var request = $.ajax({
        url: partUrl,
		type: 'get',
        async: false
    }).done (function (data) {
    	callback(data);
    }).fail(function () {
        callback('<div class="col-12 not-found-message"><h1>404, pagina niet gevonden.</h1>Klik <a href="' + baseUrl + '">hier</a> om terug naar de homepagina te gaan.</div>');
    });
}

function get_pagecontent(callback) {
	$.ajax({
		url: baseUrl+'application/'+pageName+'/',
		type: 'get',
        async: true
	}).done(function (data) {
        callback(JSON.parse(data));
	});
}

function load_maincontent(element) {
    get_pagecontent((function (content) {
        var template = element.html(),
            templateScript = Handlebars.compile(template, {noEscape: true}),
            html = templateScript(content);
        element.html(html);
    }));
}

function add_to_cart(prdId, amount, price) {
    var cookieCart = Cookies.get('cart'),
        priceFloat = parseFloat(price) * amount;
        priceFloat = parseFloat(priceFloat.toFixed(2));

    if (typeof cookieCart == 'undefined') {
        cart = {user: parseInt(user.user_id), amount: amount, total: priceFloat, products: []};
    } else {
        cart = JSON.parse(cookieCart);

        cart.amount = cart.amount + amount;

        var newTotal = cart.total + priceFloat;
        cart.total = parseFloat(newTotal.toFixed(2));
    }

    if (typeof cart.products[prdId] == 'undefined') {
        cart.products[prdId] = amount;
    } else {
        cart.products[prdId] = cart.products[prdId] + amount;
    }

    Cookies.set('cart', cart);

    update_cart_amount(cart.amount);
}

function update_cart_amount(amount) {
    $('#cartAmount').html(amount).addClass('active');
}