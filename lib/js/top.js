$(document).ready(function () {
    //url_parse();

    Handlebars.registerHelper('pageScript', function () {
        var returnVar = function () {
            var scriptLink = '';
            $.ajax({
                url: baseUrl + 'lib/js/' + pageName + 'script.js',
                type: 'HEAD',
                async: false,
                success: function () {
                    scriptLink = '<script src="/computershop/lib/js/' + pageName + 'script.js"></script>';
                }
            });
            return scriptLink;
        }();
        return returnVar;
    });
});

function url_parse()
{
    var websiteName = 'computershop',
        location = document.location.href
    nameIndex = location.indexOf(websiteName) + websiteName.length + 1;

    baseUrl = location.substr(0, nameIndex);

    var varStr = location.replace(baseUrl, ''),
        urlVars = varStr.split('/'),
        count = urlVars.length;

    pageVars = {};

    if (count > 0 && urlVars[count-1].length > 0) {
        for (var i = 0; i < count; i++) {
            if (urlVars[i]) {
                if (i == 0) {
                    pageName = urlVars[i].replace(/[|&;$%@#"<>()+,]/g, "");
                } else {
                    pageVars[i] = urlVars[i].replace(/[|&;$%@#"<>()+,]/g, "");
                }
            }
        }
    } else {
        pageName = 'home';
    }

}